/*
 Name:		bird01.ino
 Created:	21.07.2019 14:15:07
 Author:	Thk
*/





#include <ESP8266WiFi.h>

#include <Adafruit_GFX.h>
#include <TM1640.h>
#include <TM16xxMatrixGFX.h>

//TM1640 module(9, 10);    // DIN=9, CLK=10
TM1640 module(13, 14);    // For ESP8266/WeMos D1-mini: DIN=D7/13/MOSI, CLK=D5/14/SCK
#define MODULE_SIZECOLUMNS 8    // number of GRD lines, will be the y-height of the display
#define MODULE_SIZEROWS 8    // number of SEG lines, will be the x-width of the display
TM16xxMatrixGFX matrix(&module, MODULE_SIZECOLUMNS, MODULE_SIZEROWS);    // TM16xx object, columns, rows

String tape = "Hacklabor ist cool";
int wait = 100; // In milliseconds

int spacer = 1;
int width = 5 + spacer; // The font width is 5 pixels

struct Eye
{
	int x;
	int y;
};
struct Eye Eyepos;

void setup() {


	// put your setup code here, to run once:
	Eyepos.x = 3;
	Eyepos.y = 3;

	Serial.begin(115200);
	delay(100);
	Serial.println("Booting");
	matrix.setIntensity(1); // Use a value between 0 and 7 for brightness
	matrix.setRotation(2);
	matrix.setMirror(true);   // set X-mirror true when using the WeMOS D1 mini Matrix LED Shield (X0=Seg1/R1, Y0=GRD1/C1)
WiFi.forceSleepBegin();
	delay(1000);



	Serial.println("Ready");
	Serial.print("IP address: ");
}

void loop() {
	for (int i = 0; i < width * tape.length() + matrix.width() - 1 - spacer; i++) {
		matrix.fillScreen(LOW);

		int letter = i / width;
		int x = (matrix.width() - 1) - i % width;
		int y = (matrix.height() - 8) / 2; // center the text vertically

		while (x + width - spacer >= 0 && letter >= 0) {
			if (letter < tape.length()) {
				matrix.drawChar(x, y, tape[letter], HIGH, LOW, 1);
			}

			letter--;
			x -= width;
		}
		matrix.write(); // Send bitmap to display
		delay(wait);
	}
	fill();



	int richtung = random(1, 100);

	if (richtung < 9) {
		struct Eye EyeposNew = chk_eye_move(richtung, Eyepos.x, Eyepos.y);
		if (EyeposNew.x != -1 && EyeposNew.y != -1) {
			Eyepos.x = EyeposNew.x;
			Eyepos.y = EyeposNew.y;
		}

	}

	drawEye(Eyepos.x, Eyepos.y, 0);
	//mled.display();


	if (random(0, 30) == 0) {
		zwinker_eye(20);
		drawEye(Eyepos.x, Eyepos.y, 0);
		//mled.display();
	}



//	mled.display();
	delay(100);

}



void zwinker_eye(int delaytime) {
	int color = 0;

	drawLinie(0, 0, 7, color, delaytime);
	drawLinie(7, 0, 7, color, delaytime);

	drawLinie(1, 0, 7, color, delaytime);
	drawLinie(6, 0, 7, color, delaytime);

	drawLinie(2, 0, 7, color, delaytime);
	drawLinie(5, 0, 7, color, delaytime);

	drawLinie(3, 0, 7, color, delaytime);
	drawLinie(4, 0, 7, color, delaytime);

	color = 1;

	drawLinie(3, 0, 7, color, delaytime);
	drawLinie(4, 0, 7, color, delaytime);

	drawLinie(2, 0, 7, color, delaytime);
	drawLinie(5, 0, 7, color, delaytime);

	drawLinie(1, 0, 7, color, delaytime);
	drawLinie(6, 0, 7, color, delaytime);

	drawLinie(0, 0, 7, color, delaytime);
	drawLinie(7, 0, 7, color, delaytime);


}

void drawLinie(int row, int collumStart, int collumEnd, int draw, int delaytime) {

	for (int i = collumStart; i <= collumEnd; i++) {
	//	mled.dot(row, i, draw);
	}
	//mled.display();
	delay(delaytime);
}

void drawEye(int x, int y, int draw) {
	for (int x1 = x; x1 < (x + 3); x1++) {
		for (int y1 = y; y1 < (y + 3); y1++) {
			//mled.dot(x1, y1, draw);
		}
	}

}

void fill() {
	for (int x = 0; x < 8; x++) {
		for (int y = 0; y < 8; y++) {
		//	mled.dot(x, y, 1);
		}
	}
}

struct Eye chk_eye_move(int direction, int x, int y) {
	struct Eye eyetemp;
	eyetemp.x = -1;
	eyetemp.y = -1;

	if (direction == 1) {  // Up + Left
		x -= 1;
		y -= 1;
	}
	if (direction == 2) { // Up + Left
		y -= 1;
	}
	if (direction == 3) {
		// Up + Left
		x += 1;
		y -= 1;
	}
	if (direction == 4) { // Up + Left
		x += 1;
	}
	if (direction == 5) { // Up + Left
		x += 1;
		y += 1;
	}
	if (direction == 6) { // Up + Left
		y += 1;
	}
	if (direction == 7) { // Up + Left
		x -= 1;
		y += 1;
	}
	if (direction == 8) { // Up + Left
		x -= 1;
	}
	if ((x < 1) || x > 4 || y < 1 || y > 4) {

		return eyetemp;
	}


	eyetemp.x = x;
	eyetemp.y = y;

	return eyetemp;
}
